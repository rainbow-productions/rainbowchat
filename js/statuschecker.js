var status = '200';
var sstatus = '200';

if (status === '200') {
  document.getElementById("status").innerHTML = "All systems operational.";
}

if (status === '200m') {
  var time = 'insert time here';
  document.getElementById("status").innerHTML = "All systems operational with scheduled maintenance at " + time;
}

if (status === '502') {
  document.getElementById("status").innerHTML = "Connection systems are down.";
  window.location.href = 'connection-error.html';
}

if (status === 'm') {
  document.getElementById("status").innerHTML = "Undergoing maintenance. Until: 9:40am";
}

if (status === '403') {
  document.getElementById("status").innerHTML = "Unable to access system status (Error 403)";
}

if (status === 'asdwn') {
  window.alert('System Status: ' + sstatus);
  document.getElementById("status").innerHTML = "All systems down and unoperational. We're working to fix the problem.";
}

// Constants
const CLIENT_ID = 'ZajKN7JVveS24x6L';
// NEVER hardcode passwords in client-side code.  This is a placeholder!
const adminPassword = ""; // REMOVE THIS IN PRODUCTION.  Use a secure backend authentication system

// Variables
let choosename = prompt("Choose your name");
let isAdmin = false; // Placeholder - needs proper authentication

// Request notification permission
Notification.requestPermission();

// Initialize Scaledrone
const drone = new ScaleDrone(CLIENT_ID, {
    data: {
        name: choosename,
        color: getRandomColor(),
    },
});

alert("Welcome, " + choosename + "!");

let members = [];

// Scaledrone event handlers
drone.on('open', error => {
    if (error) {
        return console.error(error);
    }
    console.log('Successfully connected to Scaledrone');

    const room = drone.subscribe('observable-room');
    room.on('open', error => {
        if (error) {
            return console.error(error);
        }
        console.log('Successfully joined room');
    });

    room.on('members', m => {
        members = m;
        updateMembersDOM();
    });

    room.on('member_join', member => {
        members.push(member);
        updateMembersDOM();
    });

    room.on('member_leave', ({ id }) => {
        const index = members.findIndex(member => member.id === id);
        if (index !== -1) { // Check if member exists before splicing
            members.splice(index, 1);
            updateMembersDOM();
        }
    });

    room.on('data', (text, member) => {
        if (member) {
            addMessageToListDOM(text, member);
        } else {
            // Message is from server
        }
    });
});

drone.on('close', event => {
    console.log('Connection was closed', event);
});

drone.on('error', error => {
    console.error(error);
});

// Function to generate a random color
function getRandomColor() {
    return '#' + Math.floor(Math.random() * 0xFFFFFF).toString(16);
}

// DOM elements
const DOM = {
    membersCount: document.querySelector('.members-count'),
    membersList: document.querySelector('.members-list'),
    messages: document.querySelector('.messages'),
    input: document.querySelector('.message-form__input'),
    form: document.querySelector('.message-form'),
};

// Event listener for sending messages
DOM.form.addEventListener('submit', sendMessage);

function sendMessage() {
    const value = DOM.input.value;
    if (value === '') {
        return;
    }
    DOM.input.value = '';
    drone.publish({
        room: 'observable-room',
        message: value,
    });
}

// ... (previous code)

// Create a DOM element for a member
function createMemberElement(member) {
    const { name, color, id } = member.clientData;
    const el = document.createElement('div');

    //Admin features - Requires proper backend authentication for this to be secure
    if (isAdmin) {
        // Add admin controls (color and name editing) -  Still needs backend integration for persistence
        // ... (code for admin controls) ...
    } else {
        el.appendChild(document.createTextNode(name));
    }

    el.className = 'member';
    el.style.color = color;
    el.dataset.memberId = id;
    return el;
}

// Update the members list in the DOM
function updateMembersDOM() {
    DOM.membersCount.innerText = `${members.length} users in room:`;
    DOM.membersList.innerHTML = ''; // Inefficient - improve with diffing if performance becomes an issue
    members.forEach(member => DOM.membersList.appendChild(createMemberElement(member)));
}

// Create a DOM element for a message
function createMessageElement(text, member) {
    const el = document.createElement('div');
    const memberEl = createMemberElement(member);
    el.appendChild(memberEl);
    el.appendChild(document.createTextNode(text));
    el.className = 'message';
    return el;
}

// Add a message to the messages list in the DOM
function addMessageToListDOM(text, member) {
    const el = DOM.messages;
    const wasTop = el.scrollTop === el.scrollHeight - el.clientHeight;
    const notificationOptions = {
        body: 'New message from ' + member.clientData.name + ': ' + text
    };

    el.appendChild(createMessageElement(text, member));

    // Play the notification sound (assuming you have a sound element with id 'notificationSound')
    const notificationSound = document.getElementById('notificationSound');
    if (notificationSound) {
        notificationSound.play();
    }


    new Notification('RainbowChat', notificationOptions);

    if (wasTop) {
        el.scrollTop = el.scrollHeight - el.clientHeight;
    }
}

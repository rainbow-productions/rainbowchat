function checkIPBan() {
  var bannedIPs = ['37.9.144.0', '']; // Add your list of banned IP addresses here
  var sstatus = 'unknown'

  // Make a request to the ipify API to retrieve the client's IP address
  fetch('https://api.ipify.org/?format=json')
    .then(response => response.json())
    .then(data => {
      var ipAddress = data.ip;

      // Perform the IP ban check
      if (bannedIPs.includes(ipAddress)) {
        // If the IP is in the banned IP list, treat it as banned
        document.getElementById("connection").innerHTML = "Connection Blocked (IP Banned)"
        window.alert('Error: Access Denied for ' + ipAddress);
        window.location.href = 'ip-banned.html';
      } else {
        // If not banned, continue with the code
        var status = '200';
        var sstatus = '';

        if (status === '200') {
          console.log('Status: 200');
          document.getElementById("connection").innerHTML = "Connecting to 'Innovation'...";
        }

        if (status === '200m') {
          var time = '9:00am';
          window.alert('Status: 200, Down for maintenance at ' + time);
          document.getElementById("connection").innerHTML = "Connecting to 'Innovation'...";
        }

        if (status === '502') {
          document.getElementById("connection").innerHTML = "Connection Failed (Bad Connection)";
          window.alert('Status: Error 502 - Bad Connection');
          window.location.href = 'connection-error.html';
        }

        if (status === 'm') {
          document.getElementById("connection").innerHTML = "Connection Blocked (Maintenance)";
          window.alert('Down For Maintenance');
          window.location.href = 'maintenance.html';
        }

        if (status === '403') {
          document.getElementById("connection").innerHTML = "Connection Blocked (Access Denied)";
          window.alert('Access Denied');
          window.location.href = 'ip-banned.html';
        }

        if (status === 'asdwn') {
          window.alert('All systems down and unoperational. We are currently working to fix the problem. Error ' + sstatus);
          document.getElementById("connection").innerHTML = "All systems down and unoperational. We're working to fix the problem.";
        }

      }
    })
    .catch(error => {
      console.log('An error occurred while retrieving the IP address: ', error);
    });
}

// Call the function to check IP ban
checkIPBan();

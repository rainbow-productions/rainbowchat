function checkPassword(server, passwordAttempt) {
    const passwords = {
      'slungus-kingdom': 'adenmayo'
    }
  
    if (passwords[server] && passwordAttempt === passwords[server]) {
      return true 
    }
  
    return false
  }
  
  function joinServer(server) {
  
    if (server === 'slungus-kingdom') {
      const passwordAttempt = prompt('Enter password for Slungus Kingdom:')
  
      if (!checkPassword(server, passwordAttempt)) {
        window.location.replace('./servers/slungus-kingdom/incorrect-password.html')
        return
      }
    }
  
    if (server === 'slungus-kingdom') {
      window.location.replace('./servers/slungus-kingdom/index.html')
    } else if (server === 'main') {
      window.location.replace('./servers/main/index.html')
    }
  }
  